# small sample size, small trees
rmarkdown::render("RLib/gwas/1-GWAS_simulatedH0.Rmd",
                 output_file = "1-GWAS_simulatedH0_simu1.html",
                 output_dir = "RLib/gwas/",
                 params = list("n" = 6, "B" = 20, "iter" = 1000,
                               "rd" = "simu1b"))

# large sample size, small trees
rmarkdown::render("RLib/gwas/1-GWAS_simulatedH0.Rmd",
                 output_file = "1-GWAS_simulatedH0_simu2.html",
                 output_dir = "RLib/gwas/",
                 params = list("n" = 100, "B" = 20, "iter" = 1000,
                               "rd" = "simu2b"))

# medium sample size, large trees
rmarkdown::render("RLib/gwas/1-GWAS_simulatedH0.Rmd",
                  output_file = "1-GWAS_simulatedH0_simu3.html",
                  output_dir = "RLib/gwas/",
                  params = list("n" = 40, "B" = 100, "iter" = 1000, 
                                "rd" = "simu3b", "permtest" = 0))
