---
title: "Checking assumptions for the tree test on a negative control example"
author: "Pierre Neuvial"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
    numbered_sections: yes
    self_contained: yes
    theme: journal
    toc: yes
    toc_float:
      collapsed: no
    df_print: paged
    code_folding: hide
params:
  n: 6
  B: 20
  iter: 20
  rd: "simutest"
  permtest: 1
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(message = FALSE, warning = FALSE)
```


# 1. Introduction

The purpose of this file is to simulate structured signals (trees) under a 
realistic null distribution setting and to assess how realistic the underlined 
assumptions are in this setting. These simulations are performed using GWAS
data on which a resampling of individuals is performed before trees 
(representative of the haplotype structure) are obtained.

Used data have been obtained from the `snpStats` Bioconductor package and are
data from the International HapMap project, concerning 603 SNPs spanning a one
megabase region on chromosome 22, in a sample of Europeans. LD are defined from
these data and constrained HAC (`adjclust`) is performed to obtain trees, using 
LD as a similarity measure.

Used library (and general settings) are listed below:

```{r loadLib, echo=TRUE}
library("snpStats")
library("adjclust")
library("future")
library("future.apply")
library("ggplot2")
library("limma")
library("reshape")
library("dplyr")
library("Hotelling")
library("ape")
library("RColorBrewer")
library("ggpubr")
source("../helpers/helper_functions.R")
# library settings
plan(multicore)
```
(in addition to the package `devtools` used at the end of the report to 
display session information).

```{r printParams}
params
```

```{r paths}
rd <- file.path("../../results/gwas/", params$rd)
if (!dir.exists(rd)) {
  dir.create(rd, recursive = TRUE)
}
```


## GWAS data

Data are loaded with:

```{r gwasData}
data("ld.example", package = "snpStats")
geno_data <- ceph.1mb[, -316]  ## drop one SNP leading to one missing LD value
n0 <- nrow(geno_data)
d0 <- ncol(geno_data)
```

`n0` is the number of Europeans in the sample (`r n0`) and `d0` is the number of
SNPs in the sample (`r d0`).

## Simulation settings

Simulation settings are set below. They include:

* `n`, the number of trees (in total, for the two conditions)
* `B`, the number of leaves in the trees (objects), corresponding to the number
of subsampled SNP drawn at random from the original SNPs
* `N`, the number of individuals, subsampled from the original European sample,
on which the trees are defined (as a fraction of `n0`)
* `iter`, the number of iterations of the simulation

```{r initialization}
n <- as.numeric(params$n)
B <- as.numeric(params$B)
iter <- as.numeric(params$iter)
N <- round(0.6 * n0)
```


# 2. Simulation

## 2.1 Principle of the simulation

The simulations will be based on this simple principle:

* the trees ($n = 2 \times$ `r n/2` trees) will be built each from $B =$ `r B`
objects, all independently generated by subsampling of $N =$ `N` individuals 
from the GWAS dataset (with initial size $n_0 \times d_0$, where $d_0 =$ 
`r d0`);

* SNP data from sampled individuals will be used to obtain an estimation of a
part of the LD matrix (based on the correlation). $B$ SNP positions, obtained 
as a contiguous interval sampled uniformly at random from the $d_0 =$ `r d0` 
initial SNP positions, will be included in the LD matrix, common to all 
individuals;
    
* the trees are obtained using `adjclust` with input the LD matrix (similarity
mode).

These simulations are performed $\mbox{iter} =$ `r iter` times in order to 
generate distributions of $p$-values and other statistics of interest and to
compare them with expected values. SNP and individual subsamples are varied
between the iterations.

The two following functions are used to, respectively, extract the SNP interval
for a given simulation and obtain the tree from a given SNP interval and a
number of individuals that have to be passed at random.
```{r simu-functions}
# extract a SNP interval from a SNP dataset `geno_data` and a number of SNP `B`
extract_snp <- function(geno_data, B) {
  stopifnot(d0 >= B)
  start_int <- sample(d0 - B, 1)
  full_int <- seq.int(from = start_int, by = 1, length.out = B)
  return(geno_data[, full_int])
}

# sample individuals at random and perform adjClust based on the LD estimation
get_tree <- function(geno_data, N) {
  subsample <- sample(n0, N)
  geno_sub <- geno_data[subsample, ]
  p <- ncol(geno_sub)
  ld_mat <- snpStats::ld(geno_sub, stats = "R.squared", depth = p - 1, 
                         symmetric = TRUE)
  diag(ld_mat) <- rep(1, p)
  if (any(is.na(ld_mat))) {
    return(list("LD" = "failed", "tree" = "failed"))
  } else {
    fit <- adjClust(ld_mat)
    return(list("LD" = ld_mat, "tree" = as.hclust(fit)))
  }
}
## Note: Some of the simulations have a bit less than `n` trees because of the
## handling of error
```


## 2.2 Tree generation

```{r generateTrees, results='hide', message=FALSE, warning=FALSE}
future_lapply(1:iter, future.seed = 1326L, FUN = function(ind) {
  geno_red <- extract_snp(geno_data, B)
  all_trees <- replicate(n, get_tree(geno_red, N), simplify = FALSE)
  all_mat <- lapply(all_trees, "[[", "LD")
  all_trees <- lapply(all_trees, "[[", "tree")
  # removed failed results
  failures <- sapply(all_trees, class) != "hclust"
  all_trees <- all_trees[!failures]
  all_mat <- all_mat[!failures]
  
  out_file <- file.path(rd, sprintf("trees_%04d.rds", ind))
  saveRDS(all_trees, file = out_file)
  out_file <- file.path(rd, sprintf("matrices_%04d.rds", ind))
  saveRDS(all_mat, file = out_file)
  invisible()
})
```


# 3. Tests

## 3.1 Treediff (our method)

```{r performTest}
treediff_time <- system.time({
  test_res <- future_lapply(1:iter, function(ind) {
    tree_file <- file.path(rd, sprintf("trees_%04d.rds", ind))
    trees <- readRDS(tree_file)
    trees1 <- trees[1:ceiling(n/2)]
    trees2 <- trees[(ceiling(n/2) + 1):length(trees)]
    tree_stats <- compute_stats(trees1, trees2, scale = FALSE)
    p <- length(tree_stats$variances)
    # cluster is used to store the simulation number; chr is not used
    tree_results <- compute_pvals(tree_stats$variances, tree_stats$numerator,
                                  cluster = rep(ind, p), chr = rep(1, p), 
                                  n1 = n/2, n2 = n/2)
    return(tree_results)
  })
})[3]
indiv_tests <- lapply(test_res, function(alist) alist$individual)
indiv_tests <- Reduce(rbind, indiv_tests)
names(indiv_tests)[match("cluster", names(indiv_tests))] <- "simulation"
indiv_tests <- indiv_tests[, - match("chr", names(indiv_tests))]
out_file <- file.path(rd, "restestindiv_gwas.txt")
write.table(indiv_tests, file = out_file, row.names = FALSE)

tree_tests <- lapply(test_res, function(alist) alist$global)
tree_tests <- Reduce(rbind, tree_tests)
names(tree_tests)[match("cluster", names(tree_tests))] <- "simulation"
tree_tests <- tree_tests[, - match("chr", names(tree_tests))]
out_file <- file.path(rd, "restestsimes_gwas.txt")
write.table(tree_tests, file = out_file, row.names = FALSE)
```


## 3.2 Matdiff

An alternative approach uses individual (moderated) Student's tests for each 
matrix entry followed by Simes' aggregation. This is the strict equivalent of
our method but performed at the matrix level instead of the tree level.

```{r performMatTest}
mattest_time <- system.time({
  mat_test_res <- future_lapply(1:iter, function(ind) {
    mat_file <- file.path(rd, sprintf("matrices_%04d.rds", ind))
    matrices <- readRDS(mat_file)
    matrices <- lapply(matrices, function(x) log(x + 1))
    mat1 <- matrices[1:ceiling(n/2)]
    mat2 <- matrices[(ceiling(n/2) + 1):length(matrices)]
    mat_stats <- compute_mat_stats(mat1, mat2, scale = FALSE)
    p <- length(mat_stats$variances)
    # cluster is used to store the simulation number; chr is not used
    mat_results <- compute_pvals(mat_stats$variances, mat_stats$numerator,
                                 cluster = rep(ind, p), chr = rep(1, p), n1 = n/2,
                                 n2 = n/2)
    return(mat_results)
  })
})[3]

mat_indiv_tests <- lapply(mat_test_res, function(alist) alist$individual)
mat_indiv_tests <- Reduce(rbind, mat_indiv_tests)
names(mat_indiv_tests)[match("cluster", names(mat_indiv_tests))] <- "simulation"
mat_indiv_tests <- mat_indiv_tests[, - match("chr", names(mat_indiv_tests))]
out_file <- file.path(rd, "resmattestindiv_gwas.txt")
write.table(mat_indiv_tests, file = out_file, row.names = FALSE)

mat_tests <- lapply(mat_test_res, function(alist) alist$global)
mat_tests <- Reduce(rbind, mat_tests)
names(mat_tests)[match("cluster", names(mat_tests))] <- "simulation"
mat_tests <- mat_tests[, - match("chr", names(mat_tests))]
out_file <- file.path(rd, "resmattestsimes_gwas.txt")
write.table(mat_tests, file = out_file, row.names = FALSE)
```


## 3.3 Hotelling's test based on trees

To compare our approach with the diagonal Hotelling's test, we also computed
Hotelling's test statistics and associated $p$-values based on treediff results:

```{r computeH}
ddl_squeezing <- function(variances, n) {
  squeezed_var <- squeezeVar(variances, df = n - 2, robust = FALSE)
  return(squeezed_var$df.prior)
}

hotelling_res <- indiv_tests %>% group_by(simulation) %>% 
  mutate("ddl" = ddl_squeezing(variances, n = n)) %>%
  mutate("dim" = length(variances)) %>%
  summarize("p" = min(dim), "nu0" = min(ddl), 
            "statistics" = sum(statistics^2) / p) %>%
  mutate("pval" = 1 - pf(statistics, df1 = p, df2 = p*(nu0 + n - 2)))
```


## 3.4 Hotelling's test based on matrices

To compare our approach with the diagonal Hotelling's test, we also computed
Hotelling's test statistics and associated $p$-values based on matdiff results:

```{r computeHM}
mhotelling_res <- mat_indiv_tests %>% group_by(simulation) %>% 
  mutate("ddl" = ddl_squeezing(variances, n = n)) %>%
  mutate("dim" = length(variances)) %>%
  summarize("p" = min(dim), "nu0" = min(ddl), 
            "statistics" = sum(statistics^2) / p) %>%
  mutate("pval" = 1 - pf(statistics, df1 = p, df2 = p*(nu0 + n - 2))) %>%
  na.omit()
```


## 3.5 Hotelling's test based on permutations

Our approach is also compared with a permutation based approach. To do it, we
used a shrinked Hotelling's statistics (that avoids the singularity problem of
the standard Hotelling's test in high-dimension). The number of different 
choices for the 1st condition is bounded by $C_n^{n/2}$, which, in our case is 
equal to `r choose(n, n/2)`. We set the number of permutations to 1,000 (1,000 
yielded to a computational time larger than 2 days for the setting $n = 40$ and 
$B = 100$). In the case where the number of different choices for the 1st
condition is larger than $B$, the number of permutations is reduced to this
value and all the different choices are used.

In addition, for the permutation test, we computed the $p$-value as 
$\frac{\# \{ \textrm{Stat}_\textrm{obs} < \textrm{Stat}_b\,:\ b =1,\, \ldots,\, \textrm{nboot} \} + 1}{\textrm{nboot}+1}$, 
where 'nboot' is the number of bootstrap sample. This slightly deviates from
what is implemented in the package **Hotelling** so as to avoid wrongly 
estimating the $p$-value as zero (see 
[Phipson & Smyth, 2010; Hemerik & Goeman, 2018]). 

```{r performPermTest, warning=FALSE, results='hide'}
if (params$permtest == 1) {
  permtest_time <- system.time({
    perm_test_res <- future_lapply(1:iter, future.seed = 1601L, function(ind) {
      mat_file <- file.path(rd, sprintf("matrices_%04d.rds", ind))
      matrices <- readRDS(mat_file)
      matrices <- lapply(matrices, function(x) log(x + 1))
      mat1 <- matrices[1:ceiling(n/2)]
      mat2 <- matrices[(ceiling(n/2) + 1):length(matrices)]
      
      perm_test <- perform_hotelling(mat1, mat2, 1000)
      return(perm_test)
    })
  })[3]
  perm_test_res <- data.frame(Reduce(rbind, perm_test_res))
  perm_test_res <- na.omit(perm_test_res)
}
```

`r ifelse(params$permtest == 1, nrow(perm_test_res), 0)` tests were performed 
over `r iter` possible tests.


## 3.6 Mantel test

In this simulation, we will perform an average of the matrices per condition, 
followed by a Mantel test. Note that Mantel's test can be used only for the
comparison of two matrices thus it is not able to account for variability within
a condition. In addition, Mantel test is a permutation test so it has a 
$p$-value lower bounded by a value depending on the number of permutations (in
our case, the number of permutation is the default argument of the used 
function, 999, so the $p$-value can not be smaller than 0.001). 

```{r computeMantel}
manteltest_time <- system.time({
  mantel_test_res <- future_sapply(1:iter, future.seed = 1628L, function(ind) {
    mat_file <- file.path(rd, sprintf("matrices_%04d.rds", ind))
    matrices <- readRDS(mat_file)
    matrices <- lapply(matrices, function(x) log(x + 1))
    mat1 <- Reduce("+", lapply(matrices[1:ceiling(n/2)], as.matrix))
    mat2 <- lapply(matrices[(ceiling(n/2) + 1):length(matrices)], as.matrix)
    mat2 <- Reduce("+", mat2)
    mantel_results <- mantel.test(mat1, mat2)$p
    
    return(mantel_results)
  })
})[3]
mantel_test_res <- data.frame("pval" = mantel_test_res)
out_file <- file.path(rd, "resmanteltest_gwas.txt")
write.table(mantel_test_res, file = out_file, row.names = FALSE)
```


# 4. Exploring results

Results are then extracted and analyzed in this section. 

## 4.1 Distribution of $p$-values

### Treediff

The distribution of $p$-values at the simulation level (one for each group of 
`r n` trees) is:

```{r pvalDistribution}
nb_sig <- sum(tree_tests$pval < 0.05) / nrow(tree_tests)
p <- ggplot(tree_tests, aes(x = pval)) + geom_histogram(bins = 20) + 
  theme_bw() + xlab("p-value") + 
  ggtitle(paste("Proportion of p-values < 0.05:", round(nb_sig * 100, 1), "%"))
p
out <- file.path(rd, "pvals_gwas_distribution.png")
ggsave(filename = out, device = "png", plot = p, width = 4, height = 3, 
       dpi = 200)
```

### Matdiff

```{r pvalDistributionMat}
nb_sig <- sum(mat_tests$pval < 0.05) / nrow(mat_tests)
p <- ggplot(mat_tests, aes(x = pval)) + geom_histogram(bins = 20) + 
  theme_bw() + xlab("p-value") + 
  ggtitle(paste("Proportion of p-values < 0.05:", round(nb_sig * 100, 1), "%"))
p
out <- file.path(rd, "pvalsMat_gwas_distribution.png")
ggsave(filename = out, device = "png", plot = p, width = 4, height = 3, 
       dpi = 200)
```


### Hotelling's test based on trees

The distribution of Hotelling's $p$-values is:

```{r pvalsH}
nb_sig <- sum(hotelling_res$pval < 0.05) / nrow(hotelling_res)
p <- ggplot(hotelling_res, aes(x = pval)) + geom_histogram(bins = 20) + 
  theme_bw() + xlab("p-value") + 
  ggtitle(paste("Proportion of p-values < 0.05:", round(nb_sig * 100, 1), "%"))
p
out <- file.path(rd, "pvalsHotelling_gwas_distribution.png")
ggsave(filename = out, device = "png", plot = p, width = 4, height = 3, 
       dpi = 200)
```


### Hotelling's test based on matrices

The distribution of Hotelling's $p$-values is:

```{r pvalsHM}
nb_sig <- sum(mhotelling_res$pval < 0.05) / nrow(mhotelling_res)
p <- ggplot(mhotelling_res, aes(x = pval)) + geom_histogram(bins = 20) + 
  theme_bw() + xlab("p-value") + 
  ggtitle(paste("Proportion of p-values < 0.05:", round(nb_sig * 100, 1), "%"))
p
out <- file.path(rd, "pvalsMHotelling_gwas_distribution.png")
ggsave(filename = out, device = "png", plot = p, width = 4, height = 3, 
       dpi = 200)
```


### Hotelling's test based on permutations

The distribution of Hotelling's $p$-values is:

```{r pvalsPerm}
if (params$permtest == 1) {
  nb_sig <- sum(perm_test_res$pval < 0.05) / nrow(perm_test_res)
  nperms <- min(1000, choose(n, ceiling(n/2)))
  p <- ggplot(perm_test_res, aes(x = pval)) + geom_histogram(bins = 20) + 
    theme_bw() + xlab("p-value") + 
    ggtitle(paste("Proportion of p-values < 0.05:", round(nb_sig * 100, 1), 
                  "%\n", nperms, "permutations used"))
  print(p)
  out <- file.path(rd, "pvalsPerms_gwas_distribution.png")
  ggsave(filename = out, device = "png", plot = p, width = 4, height = 3, 
        dpi = 200)
}
```


### Mantel test

```{r pvalMantel}
nb_sig <- sum(mantel_test_res$pval < 0.05) / nrow(mantel_test_res)
p <- ggplot(mantel_test_res, aes(x = pval)) + geom_histogram(bins = 20) + 
  theme_bw() + xlab("p-value") + xlim(0, 1) +
  ggtitle(paste("Proportion of p-values < 0.05:", round(nb_sig * 100, 1), "%"))
p
out <- file.path(rd, "pvalsMantel_gwas_distribution.png")
ggsave(filename = out, device = "png", plot = p, width = 4, height = 3, 
       dpi = 200)
```


## 4.2 Empirical cumulative density distribution

```{r ecdfTreediff}
test_names <- c("tree test", "matrix test", "diagonal tree Hotelling", 
                "diagonal mat. Hotelling", "permutation test", "Mantel test")
test_palette <- RColorBrewer::brewer.pal(length(test_names), "Set1")
if (params$permtest == 1) {
  nb_tests <- c(nrow(tree_tests), nrow(mat_tests), nrow(hotelling_res),
                nrow(mhotelling_res), nrow(perm_test_res), 
                nrow(mantel_test_res))
  pvals <- c(tree_tests$pval, mat_tests$pval, hotelling_res$pval, 
            mhotelling_res$pval, perm_test_res$pval, mantel_test_res$pval)
  sel_names <- test_names
} else {
  nb_tests <- c(nrow(tree_tests), nrow(mat_tests), nrow(hotelling_res),
                nrow(mhotelling_res), nrow(mantel_test_res))
  pvals <- c(tree_tests$pval, mat_tests$pval, hotelling_res$pval, 
             mhotelling_res$pval, mantel_test_res$pval)
  sel_names <- test_names[- 5]
}
df <- data.frame("pval" = pvals, "type" = rep(sel_names, nb_tests))
sel_palette <- test_palette[match(sel_names, test_names)]

p <- ggplot(df, aes(x = pval, colour = type)) + 
  geom_abline(slope = 1, intercept = 0, linetype = "dashed") + stat_ecdf() +
  theme_bw() + xlab("p-value") + theme(axis.title.y = element_blank()) +
  scale_color_manual("name" = "Test", values = sel_palette, 
                     breaks = sel_names)
p

legend_p <- as_ggplot(get_legend(p))
out <- file.path(rd, "pvalsECDF_gwas_legend.png")
ggsave(filename = out, device = "png", plot = legend_p, width = 2, height = 3,
       dpi = 200)
p <- p + theme(legend.position = "none")
out <- file.path(rd, "pvalsECDF_gwas.png")
ggsave(filename = out, device = "png", plot = p, width = 4, height = 4, 
       dpi = 200)
```


## 4.3 Distribution of test statistics

### Treediff

Statistics at SNP pair level are supposed to follow (under the null assumption)
a Student law (with adjusted degree of freedom due to the squeezing procedure).
Since, for a given simulation, the adjustment of degree of freedom is different,
we generated, for each simulation, 10 random variables drawn from the proper
Student distribution. The obtained and theoretical distributions of statistics
are thus plotted together.

```{r statDistribution, warning = FALSE}
add_df <- tapply(indiv_tests$variances, indiv_tests$simulation, 
                 function(all_vars) {
                   squeezeVar(all_vars, df = n - 2, robust = FALSE)$df.prior
                 })
set.seed(13041720)
theoretical <- sapply(add_df, function(adf) rt(10, df = n - 2 + adf))
theoretical <- as.vector(theoretical)

df <- data.frame("statistic" = c(indiv_tests$statistics, theoretical),
                 "type" = rep(c("observed", "theoretical"), 
                              c(nrow(indiv_tests), length(theoretical))))
p <- ggplot(df, aes(x = statistic)) + geom_density(aes(colour = type)) +
  theme_bw() + xlim(-10, 10) + xlab("test statistics") +
  scale_color_brewer(name = "", palette = 6, type = "qual")
p
legend_p <- as_ggplot(get_legend(p))

p <- p + theme(legend.position = "none")
out <- file.path(rd, "stat_gwas_distribution.png")
ggsave(filename = out, device = "png", plot = p, width = 4, height = 3,
       dpi = 200)
out <- file.path(rd, "stat_gwas_distribution_legend.png")
ggsave(filename = out, device = "png", plot = legend_p, width = 2, height = 3,
       dpi = 200)
```

A quantile-quantile plot is performed to check the adjustment between the 
empirical distribution of the statistics and the theoretical distribution (here,
the latter is approximated by a $t$ distribution with the average degrees of 
freedom):

```{r QQplot}
p <- ggplot(indiv_tests, aes(sample = statistics)) + 
  stat_qq(distribution = qt, dparams = n - 2 + mean(add_df)) +
  stat_qq_line() + geom_abline(slope = 1, intercept = 0, colour = "red") +
  xlim(-5, 5) + ylim(-5, 5) + theme_bw()
p
out <- file.path(rd, "stat_gwas_qqplot.png")
ggsave(filename = out, device = "png", plot = p, width = 8, height = 8,
       dpi = 200)
```

The validity of the approximation of the theoretical by a $t$ distribution with
the average degrees of freedom is checked by another qqplot that uses the 
empirical distribution given in `theoretical`:

```{r QQplot2}
len <- length(theoretical)
theoretical <- approx(1L:len, sort(theoretical), 
                      n = length(indiv_tests$statistics))$y
df <- data.frame(sample = sort(indiv_tests$statistics), 
                 theoretical = theoretical)
p <- ggplot(df, aes(x = theoretical, y = sample)) + geom_point() + 
  geom_abline(slope = 1, intercept = 0, colour = "red") + xlim(-5, 5) + 
  ylim(-5, 5) + theme_bw()
p
out <- file.path(rd, "stat_gwas_qqplot_theoretical.png")
ggsave(filename = out, device = "png", plot = p, width = 8, height = 8,
       dpi = 200)
```


### Matdiff

The same theoretical distribution is used for the test based on matrix entries.

```{r statDistributionMat, warning = FALSE}
add_df <- tapply(mat_indiv_tests$variances, mat_indiv_tests$simulation, 
                 function(all_vars) {
                   squeezeVar(all_vars, df = n - 2, robust = FALSE)$df.prior
                 })
set.seed(13041720)
theoretical <- sapply(add_df, function(adf) rt(10, df = n - 2 + adf))
theoretical <- as.vector(theoretical)

df <- data.frame("statistic" = c(mat_indiv_tests$statistics, theoretical),
                 "type" = rep(c("observed", "theoretical"), 
                              c(nrow(mat_indiv_tests), length(theoretical))))
p <- ggplot(df, aes(x = statistic)) + geom_density(aes(colour = type)) +
  theme_bw() + xlim(-10, 10) + xlab("test statistics") +
  scale_color_brewer(name = "", palette = 6, type = "qual")
p
legend_p <- as_ggplot(get_legend(p))

p <- p + theme(legend.position = "none")
out <- file.path(rd, "statMat_gwas_distribution.png")
ggsave(filename = out, device = "png", plot = p, width = 4, height = 3,
       dpi = 200)
out <- file.path(rd, "statMat_gwas_distribution_legend.png")
ggsave(filename = out, device = "png", plot = legend_p, width = 2, height = 3,
       dpi = 200)
```


### Hotelling's test based on trees

Statistics are supposed to follow (under the null assumption) a Fisher law with
$p = \frac{B(B-1)}{2}$ and $p(n-2+\nu_0)$ ddl (where $\nu_0$ is the adjusted 
degree of freedom due to the squeezing procedure). The observed and the 
(simulated) theoretical distributions of statistics are plotted together.

```{r statDistributionH, warning = FALSE}
set.seed(28091550)
p <- (B * (B-1)) / 2
theoretical <- sapply(hotelling_res$nu0, function(adf)
  rf(10, df1 = p, df2 = p * (n - 2 + adf)))
theoretical <- as.vector(theoretical)

df <- data.frame("statistic" = c(hotelling_res$statistics, theoretical),
                 "type" = rep(c("observed", "theoretical"), 
                              c(nrow(hotelling_res), length(theoretical))))
p <- ggplot(df, aes(x = statistic)) + geom_density(aes(colour = type)) +
  theme_bw() + xlim(-10, 10) + xlab("test statistics") +
  scale_color_brewer(name = "", palette = 6, type = "qual") + 
  theme(legend.position = "none")
p

out <- file.path(rd, "statHotelling_gwas_distribution.png")
ggsave(filename = out, device = "png", plot = p, width = 4, height = 3,
       dpi = 200)
```


### Hotelling's test based on matrices

Statistics are supposed to follow (under the null assumption) a Fisher law with
$p = \frac{B(B-1)}{2}$ and $p(n-2+\nu_0)$ ddl (where $\nu_0$ is the adjusted 
degree of freedom due to the squeezing procedure). The observed and the 
(simulated) theoretical distributions of statistics are plotted together.

```{r statDistributionHM, warning = FALSE}
set.seed(28091550)
p <- (B * (B-1)) / 2
theoretical <- sapply(mhotelling_res$nu0, function(adf)
  rf(10, df1 = p, df2 = p * (n - 2 + adf)))
theoretical <- as.vector(theoretical)

df <- data.frame("statistic" = c(mhotelling_res$statistics, theoretical),
                 "type" = rep(c("observed", "theoretical"), 
                              c(nrow(mhotelling_res), length(theoretical))))
p <- ggplot(df, aes(x = statistic)) + geom_density(aes(colour = type)) +
  theme_bw() + xlim(-10, 10) + xlab("test statistics") +
  scale_color_brewer(name = "", palette = 6, type = "qual") + 
  theme(legend.position = "none")
p

out <- file.path(rd, "statMHotelling_gwas_distribution.png")
ggsave(filename = out, device = "png", plot = p, width = 4, height = 3,
       dpi = 200)
```


## 4.4 Computational times

We computed the computational time for all simulations and all methods:

```{r computTime}
if (params$permtest == 1) {
  sel_names <- c("tree test", "matrix test", "permutation test", "Mantel test")
  sel_names <- factor(sel_names, levels = sel_names, ordered = TRUE)
  sel_palette <- test_palette[match(sel_names, test_names)]
  df <- data.frame("name" = sel_names,
                  "time" = c(treediff_time, mattest_time, permtest_time, 
                              manteltest_time))
  p <- ggplot(df, aes(x = name, y = time, fill = name)) +
    geom_bar(stat = "identity") + theme_bw() + ylab("computational time (s)") +
    scale_fill_manual("name" = "Test", values = sel_palette, 
                      breaks = sel_names) +
    theme(axis.title.x = element_blank(), legend.position = "none")
  print(p)

  out <- file.path(rd, "compTime_gwas.png")
  ggsave(filename = out, device = "png", plot = p, width = 4, height = 3, 
        dpi = 200)
}
```

and without the permutation test:
```{r computTime2}
sel_names <- c("tree test", "matrix test", "Mantel test")
sel_names <- factor(sel_names, levels = sel_names, ordered = TRUE)
sel_palette <- test_palette[match(sel_names, test_names)]
df <- data.frame("name" = sel_names,
                 "time" = c(treediff_time, mattest_time, manteltest_time))
p <- ggplot(df, aes(x = name, y = time, fill = name)) +
  geom_bar(stat = "identity") + theme_bw() + ylab("computational time (s)") +
  scale_fill_manual("name" = "Test", values = sel_palette, breaks = sel_names) +
  theme(axis.title.x = element_blank(), legend.position = "none")
p

out <- file.path(rd, "compTimeNoPermTest_gwas.png")
ggsave(filename = out, device = "png", plot = p, width = 3, height = 3, 
       dpi = 200)
```


# References

Hemerik, J. & Goeman, J. (2018). Exact testing with random permutations. *Test*,
**27**(4), 811-825. https://doi.org/10.1007/s11749-017-0571-1

Phipson, B. & Smyth, G.K. (2010) Permutation p-values should never be zero: 
calculating exact p-values when permutations are randomly drawn. *Statistical
Applications in Genetics and Molecular Biology*, **9**(1). 
https://doi.org/10.2202/1544-6115.1585

Randriamihamison, N., Vialaneix, N., & Neuvial, P. (2020). Applicability and
interpretability of Ward's hierarchical agglomerative clustering with or without
contiguity constraints. *Journal of Classification*, **38**: 363-389.
https://doi.org/10.1007/s00357-020-09377-y


# Session information

```{r sessionInfo}
devtools::session_info()
```
