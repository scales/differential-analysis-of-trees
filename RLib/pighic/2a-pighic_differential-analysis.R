# This script aims at performing the statistical test by constructing a 
# dendrogram for every cluster of every chromosome and every sample and by
# using the six dendrograms of the different samples for the cluster as input
# of the computation. Results under permutation of the conditions are also
# obtained.
###############################################################################
## Inputs: normalized counts and clusters generated by 
### 1-pighic_norm-clustering.R
## Output: 
### a data table providing for all clusters in the 18 chromosomes, the
### p-values (raw and BH corrected) of the test and the test statistics
### detailed results with p-values at the bin pair levels are also saved
## Saved: 
### cluster results are saved in results/pighic/restestsimes_200000_norm.txt
### cluster results under permutation are saved in 
### results/pighic/restestsimes_200000_norm-permuted.txt
### detailed results are saved in results/pighic/restestindiv_200000_norm.txt
### and results/pighic/restestindiv_200000_norm-permuted.txt
## Run from the root directory: in parallel on SGE cluster (6 cores, 10Go RAM)
###############################################################################

## Required library
library("future.apply")
library("adjclust")
source("RLib/helpers/helper_functions.R")

## Global settings
plan(multiprocess)
options(future.globals.maxSize = 2500 * 1024^2)
dd <- "data/pighic"
rd <- "results/pighic"
chr <- 1:18

## Processing
### list all files with normalized counts and clusters by chromosome
norm_matrices <- paste0("chr", chr, "_200000_normcounts.txt")
clust_results <- paste0("chr", chr, "_200000_norm-clust.txt")

### loop over chromosomes and clusters to obtain bin pair statistics
out <- future_sapply(seq_along(norm_matrices), simplify = FALSE, 
                     future.seed = 1708L, FUN = function(ind) {
  print(paste0("processing... ", dd, "/", norm_matrices[ind]))

  #### reading count matrix
  cur_mat <- read.table(file.path(dd, norm_matrices[ind]), header = TRUE, 
                        sep = " ", stringsAsFactors = FALSE)
  #### reading clusters
  cur_clust <- read.table(file.path(rd, clust_results[ind]), row.names = 1)
  all_clusters <- unique(cur_clust[, 1])
  
  all_res <- sapply(all_clusters, function(ac) {
    #### for every cluster, create a dendrogram for every sample
    selected <- rownames(cur_clust)[cur_clust == ac]
    red_mat <- ((cur_mat$b1 %in% selected) & (cur_mat$b2 %in% selected))
    red_mat <- cur_mat[red_mat, ]
    
    adjclust_out <- sapply(1:6, function(arep) {
      ##### create a merged squared similarity matrix with normalized counts
      mat_crep <- matrix(0, ncol = length(selected), nrow = length(selected))
      match1 <- match(red_mat$b1, selected)
      match2 <- match(red_mat$b2, selected)
      mat_crep[cbind(match1, match2)] <- red_mat[, arep + 1]
      mat_crep[cbind(match2, match1)] <- red_mat[, arep + 1]
      
      out <- adjClust(log(mat_crep + 1), type = "similarity")
      class(out) <- "hclust"
      return(out)
    }, simplify = FALSE)
    cond1 <- grep("90", colnames(red_mat[, 2:7]))
    cond2 <- grep("110", colnames(red_mat[, 2:7]))
    trees1 <- adjclust_out[cond1]
    trees2 <- adjclust_out[cond2]
    
    #### obtain statistics on bin pairs
    outn <- compute_stats(trees1, trees2, scale = FALSE)
    outn <- data.frame("variances" = outn$variances,
                       "numerator" = outn$numerator,
                       "cluster" = rep(ac, length(outn$variances)))
    #### redo after permutations of conditions
    repeat {
      cond1 <- sample(1:6, 3, replace = FALSE)
      if ((length(intersect(cond1, c(1, 3, 5))) != 0) & 
          (length(intersect(cond1, c(1, 3, 5))) != 3)) break
    }
    cond2 <- setdiff(1:6, cond1)
    trees1 <- adjclust_out[cond1]
    trees2 <- adjclust_out[cond2]
    outp <- compute_stats(trees1, trees2, scale = FALSE)
    outp <- data.frame("variances" = outp$variances, 
                       "numerator" = outp$numerator,
                       "cluster" = rep(ac, length(outp$variances)))
    
    #### combine all results
    out <- data.frame(rbind(outn, outp), 
                      "type" = rep(c("direct", "permuted"),
                                   c(nrow(outn), nrow(outp))))
    
    return(out)
  }, simplify = FALSE)
  all_res <- Reduce(rbind, all_res)
  all_res$chr <- rep(chr[ind], nrow(all_res))

  return(all_res)
})
out <- Reduce(rbind, out)
outd <- out[out$type == "direct", c("variances", "numerator", "cluster", "chr")]
outp <- out[out$type == "permuted", c("variances", "numerator", "cluster", "chr")]

### direct results (no permutation)
res_direct <- compute_pvals(outd$variances, outd$numerator, outd$cluster, 
                            outd$chr, n1 = 3, n2 = 3)
outfile <- paste0("restestindiv_200000_norm.txt")
write.table(res_direct$individual, file = file.path(rd, outfile), row.names = FALSE)
outfile <- paste0("restestsimes_200000_norm.txt")
write.table(res_direct$global, file = file.path(rd, outfile), row.names = FALSE)

### permuted results
res_perm <- compute_pvals(outp$variances, outp$numerator, outp$cluster, 
                          outp$chr, n1 = 3, n2 = 3)
outfile <- paste0("restestindiv_200000_norm-permuted.txt")
write.table(res_perm$individual, file = file.path(rd, outfile), row.names = FALSE)
outfile <- paste0("restestsimes_200000_norm-permuted.txt")
write.table(res_perm$global, file = file.path(rd, outfile), row.names = FALSE)

## Session information
# devtools::session_info()
# ─ Session info ───────────────────────────────────────────────────────────────
# setting  value                       
# version  R version 4.0.2 (2020-06-22)
# os       CentOS Linux 7 (Core)       
# system   x86_64, linux-gnu           
# ui       X11                         
# language (EN)                        
# collate  en_US.UTF-8                 
# ctype    en_US.UTF-8                 
# tz       Europe/Paris                
# date     2023-04-11                  
# 
# ─ Packages ───────────────────────────────────────────────────────────────────
# ! package      * version  date       lib source        
# P adjclust     * 0.5.7    2018-09-26 [?] CRAN (R 4.0.2)
# P assertthat     0.2.1    2019-03-21 [?] CRAN (R 4.0.2)
# P backports      1.1.9    2020-08-24 [?] CRAN (R 4.0.2)
# P blob           1.2.1    2020-01-20 [?] CRAN (R 4.0.2)
# P broom          0.7.5    2021-02-19 [?] CRAN (R 4.0.2)
# P callr          3.4.4    2020-09-07 [?] CRAN (R 4.0.2)
# P capushe        1.1.1    2016-04-19 [?] CRAN (R 4.0.2)
# P cellranger     1.1.0    2016-07-27 [?] CRAN (R 4.0.2)
# P cli            2.0.2    2020-02-28 [?] CRAN (R 4.0.2)
# P codetools      0.2-16   2018-12-24 [?] CRAN (R 4.0.2)
# P colorspace     2.0-0    2020-11-11 [?] CRAN (R 4.0.2)
# P corpcor      * 1.6.10   2021-09-16 [?] CRAN (R 4.0.2)
# P crayon         1.3.4    2017-09-16 [?] CRAN (R 4.0.2)
# P DBI            1.1.0    2019-12-15 [?] CRAN (R 4.0.2)
# P dbplyr         1.4.4    2020-05-27 [?] CRAN (R 4.0.2)
# P desc           1.2.0    2018-05-01 [?] CRAN (R 4.0.2)
# P devtools       2.1.0    2019-07-06 [?] CRAN (R 4.0.2)
# P digest         0.6.25   2020-02-23 [?] CRAN (R 4.0.2)
# P dplyr        * 1.0.2    2020-08-18 [?] CRAN (R 4.0.2)
# P ellipsis       0.3.1    2020-05-15 [?] CRAN (R 4.0.2)
# P fansi          0.4.1    2020-01-08 [?] CRAN (R 4.0.2)
# P forcats      * 0.5.1    2021-01-27 [?] CRAN (R 4.0.2)
# P fs             1.5.0    2020-07-31 [?] CRAN (R 4.0.2)
# P future       * 1.14.0   2019-07-02 [?] CRAN (R 4.0.2)
# P future.apply * 1.3.0    2019-06-18 [?] CRAN (R 4.0.2)
# P generics       0.0.2    2018-11-29 [?] CRAN (R 4.0.2)
# P ggplot2      * 3.3.3    2020-12-30 [?] CRAN (R 4.0.2)
# P globals        0.12.4   2018-10-11 [?] CRAN (R 4.0.2)
# P glue           1.4.2    2020-08-27 [?] CRAN (R 4.0.2)
# P gtable         0.3.0    2019-03-25 [?] CRAN (R 4.0.2)
# P haven          2.3.1    2020-06-01 [?] CRAN (R 4.0.2)
# P hms            0.5.3    2020-01-08 [?] CRAN (R 4.0.2)
# P Hotelling    * 1.0-8    2021-09-09 [?] CRAN (R 4.0.2)
# P httr           1.4.2    2020-07-20 [?] CRAN (R 4.0.2)
# P jsonlite       1.7.1    2020-09-07 [?] CRAN (R 4.0.2)
# P lattice        0.20-41  2020-04-02 [?] CRAN (R 4.0.2)
# P lifecycle      0.2.0    2020-03-06 [?] CRAN (R 4.0.2)
# P limma        * 3.44.3   2020-06-12 [?] Bioconductor  
# P listenv        0.7.0    2018-01-21 [?] CRAN (R 4.0.2)
# P lubridate      1.7.9.2  2020-11-13 [?] CRAN (R 4.0.2)
# P magrittr       1.5      2014-11-22 [?] CRAN (R 4.0.2)
# P MASS           7.3-51.6 2020-04-26 [?] CRAN (R 4.0.2)
# P Matrix         1.2-18   2019-11-27 [?] CRAN (R 4.0.2)
# P matrixStats    0.56.0   2020-03-13 [?] CRAN (R 4.0.2)
# P memoise        1.1.0    2017-04-21 [?] CRAN (R 4.0.2)
# P modelr         0.1.8    2020-05-19 [?] CRAN (R 4.0.2)
# P munsell        0.5.0    2018-06-12 [?] CRAN (R 4.0.2)
# P pillar         1.4.6    2020-07-10 [?] CRAN (R 4.0.2)
# P pkgbuild       1.1.0    2020-07-13 [?] CRAN (R 4.0.2)
# P pkgconfig      2.0.3    2019-09-22 [?] CRAN (R 4.0.2)
# P pkgload        1.1.0    2020-05-29 [?] CRAN (R 4.0.2)
# P prettyunits    1.1.1    2020-01-24 [?] CRAN (R 4.0.2)
# P processx       3.4.4    2020-09-03 [?] CRAN (R 4.0.2)
# P ps             1.3.4    2020-08-11 [?] CRAN (R 4.0.2)
# P purrr        * 0.3.4    2020-04-17 [?] CRAN (R 4.0.2)
# P R6             2.4.1    2019-11-12 [?] CRAN (R 4.0.2)
# P Rcpp           1.0.5    2020-07-06 [?] CRAN (R 4.0.2)
# P readr        * 1.4.0    2020-10-05 [?] CRAN (R 4.0.2)
# P readxl         1.3.1    2019-03-13 [?] CRAN (R 4.0.2)
# P remotes        2.1.0    2019-06-24 [?] CRAN (R 4.0.2)
# renv           0.13.0   2021-02-24 [1] CRAN (R 4.0.2)
# P reprex         1.0.0    2021-01-27 [?] CRAN (R 4.0.2)
# P rlang          0.4.7    2020-07-09 [?] CRAN (R 4.0.2)
# P rprojroot      1.3-2    2018-01-03 [?] CRAN (R 4.0.2)
# P rstudioapi     0.11     2020-02-07 [?] CRAN (R 4.0.2)
# P rvest          0.3.6    2020-07-25 [?] CRAN (R 4.0.2)
# P scales         1.1.1    2020-05-11 [?] CRAN (R 4.0.2)
# P sessioninfo    1.1.1    2018-11-05 [?] CRAN (R 4.0.2)
# P stringi        1.5.3    2020-09-09 [?] CRAN (R 4.0.2)
# P stringr      * 1.4.0    2019-02-10 [?] CRAN (R 4.0.2)
# P testthat       2.3.2    2020-03-02 [?] CRAN (R 4.0.2)
# P tibble       * 3.0.3    2020-07-10 [?] CRAN (R 4.0.2)
# P tidyr        * 1.1.2    2020-08-27 [?] CRAN (R 4.0.2)
# P tidyselect     1.1.0    2020-05-11 [?] CRAN (R 4.0.2)
# P tidyverse    * 1.3.0    2019-11-21 [?] CRAN (R 4.0.2)
# P usethis        1.5.1    2019-07-04 [?] CRAN (R 4.0.2)
# P vctrs          0.3.4    2020-08-29 [?] CRAN (R 4.0.2)
# P withr          2.4.1    2021-01-26 [?] CRAN (R 4.0.2)
# P xml2           1.3.2    2020-04-23 [?] CRAN (R 4.0.2)
# 
# [1] /work/nvilla/treediff/renv/library/R-4.0/x86_64-pc-linux-gnu
# [2] /tmp/RtmpaGSlxZ/renv-system-library