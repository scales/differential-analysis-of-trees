---
title: "Analysis of results for simulations from PigHiC data"
author: "Nathalie Vialaneix"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
    numbered_sections: yes
    self_contained: yes
    theme: journal
    toc: yes
    toc_float:
      collapsed: no
    df_print: paged
    code_folding: hide
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, warning = FALSE, message = FALSE)
```

This notebook analyses the results of simulations based on the PigHiC data and
produced in the scripts `2d-pighic_fakeBR1cond`, `2e-pighic_fakeBR2cond`, 
`2f-pighic_fakeBR2cond-all`, `2g-pighic_fakeTR`, and `2h-pighic_fakeTR-all`.


```{r loadLib}
library("reshape")
library("dplyr")
library("ggplot2")
library("RColorBrewer")
source("../helpers/helper_functions.R")
```


# Loading results

```{r loadRes}
fakeBR1cond <- read.table("../../results/pighic/fakeBR1cond_200000_norm_chr1_nb-simu500.txt",
                          header = TRUE)
fakeBR2cond <- read.table("../../results/pighic/fakeBR2cond_200000_norm_chr1_nb-simu500.txt",
                          header = TRUE)
fakeBR2condAll <- read.table("../../results/pighic/fakeBR2cond-all_200000_norm_chr1_nb-simu500.txt",
                             header = TRUE)
fakeTR <- read.table("../../results/pighic/fakeTR_200000_norm_chr1_nb-simu500.txt", 
                     header = TRUE)
fakeTRAll <- read.table("../../results/pighic/fakeTR-all_200000_norm_chr1_nb-simu500.txt", 
                        header = TRUE)
```


# $p$-value analysis

First, the histograms of $p$-values are displayed:
```{r pvalHist, fig.width=10}
all_pvals <- rbind(fakeTR, fakeTRAll, fakeBR1cond, fakeBR2cond, fakeBR2condAll)
settings <- rep(c("TR", "TRAll", "BR1cond", "BR2cond", "BR2condAll"),
                c(nrow(fakeTR), nrow(fakeTRAll), nrow(fakeBR1cond), 
                  nrow(fakeBR2cond), nrow(fakeBR2condAll)))
all_pvals <- data.frame(all_pvals, "setting" = settings)
all_pvals <- reshape::melt(all_pvals, id.vars = c("simu", "setting"))
names(all_pvals) <- c("simu", "setting", "method", "pval")
all_pvals <- all_pvals %>% group_by(simu, setting, method) %>%
  dplyr::mutate(adj.pval = p.adjust(pval, method = "BH"))
all_pvals$setting <- factor(all_pvals$setting,
                            levels = c("TR", "TRAll", "BR1cond", "BR2cond", 
                                       "BR2condAll"),
                            ordered = TRUE)
all_pvals$method <- sapply(all_pvals$method == "treediff", ifelse, 
                           yes = "tree test", no = "matrix test")
all_pvals$method <- factor(all_pvals$method,
                           levels = c("tree test", "matrix test"),
                           ordered = TRUE)
all_pvals$decision <- ifelse(all_pvals$setting %in% c("TR", "TRAll"), 
                             all_pvals$pval, all_pvals$adj.pval)
sign_results <- all_pvals %>% group_by(setting, method) %>%
  dplyr::summarise(nbsig = mean(decision < 0.05))
sign_results$legend <- paste(round(sign_results$nbsig * 100, 1),  
                             "% significant\ntests (risk 5%)")
p <- ggplot(all_pvals, aes(x = pval)) + geom_histogram() + 
  facet_grid(method ~ setting) + theme_bw() + 
  geom_text(data = sign_results, x = 0.5, y = 35000, aes(label = legend)) +
  xlab("p-value")
p
outfile <- "../../results/pighic/pighic_simulated_pvalhist.png"
ggsave(outfile, plot = p, width = 22, height = 10, units = "cm")
```

Then, the $p$-value ECDF (for the three settings and all the methods) are 
displayed on the same figure:
```{r ECDF}
test_palette <- RColorBrewer::brewer.pal(2, "Set1")
p <- ggplot(all_pvals, aes(x = pval, colour = method, linetype = setting)) +
  geom_abline(slope = 1, intercept = 0, linetype = "dashed") + stat_ecdf() +
  theme_bw() + xlab("p-value") + scale_colour_manual(values = test_palette) +
  theme(axis.title.y = element_blank())
p
outfile <- "../../results/pighic/pighic_simulated_pvalecdf.png"
ggsave(outfile, plot = p, width = 14, height = 10, units = "cm")
```

# Session information

```{r sessionInfo}
devtools::session_info()
```
